import React, { ReactNode } from 'react';
import styled from "styled-components";

export const Mod = styled.div`
position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5); /* Півпрозорий чорний колір */
  display: flex;
  justify-content: center;
  align-items: center;`

interface Props{
    children: ReactNode;
    onClick: (e: React.MouseEvent) => void;
}

function Modal({children, onClick}:Props) {
  return (
    <Mod className='modal-overlay' onClick={onClick}>{children}</Mod>
  )
}

export default Modal