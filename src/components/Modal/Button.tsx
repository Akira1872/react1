import styled from "styled-components";

export const Btn = styled.button`
display: flex;
width: 192px;
height: 48px;
padding: 16px 20px;
justify-content: center;
align-items: center;
gap: 12px;
flex-shrink: 0;
border-radius: 8px;
background: #8A33FD;
color: #FFF;

    text-align: center;
    font-family: Roboto;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
    cursor: pointer;`;

interface Props{
    onClick: ()=>void;
    children: string;
    className?: string;
}

const Button = ({onClick, children, className}: Props) => {
  return (
    <Btn className={className} onClick={onClick}>{children}</Btn>
  )
}

export default Button;