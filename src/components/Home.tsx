import React, { useState, useEffect } from "react";
import Banner from "./Banner/Banner";
import EventSection from "./Events/EventSection";
import Cards from "./Card/CardBox";
import FooterContainer from "./Footer/FooterContainer";

interface Props{
  onClick: (artikel:string)=>void;
  firstClick: (artikel:string)=>void;
}

function Home({onClick, firstClick}:Props) {
  return (
    <div className="container">
        <Banner/>
        <EventSection/>
        <Cards onClick={onClick} firstClick={firstClick}/> 
        <FooterContainer/>
    </div>
  );
}


export default Home;