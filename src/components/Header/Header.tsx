import React, {useEffect} from "react";
import HeaderList from "./Headerlist";
import Inst from "./Insta";
import Threads from "./Threads";
import Cart from "../Card/Cart";
import Star from "../Card/Star";
import styled from "styled-components";
import { Link } from 'react-router-dom';



export const Head = styled.header`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 85vw;
  margin: 20px 0 20px;
`;

const listItems = ["Home", "Shop", "Event", "Service", "Contact"];
const toPaths = ["/", "/shop", "/event", "/service", "/contact"];


interface Props {
  selectedStarCount: number;
  selectedModalCount: number;
}

function Header({ selectedStarCount, selectedModalCount }: Props) {

  return (
    <Head>
      <Link to="/cart">
      <Cart colored={false} />
      </Link>
      <div>{selectedModalCount}</div>
      <Link to="/star">
      <Star colored={false} onClick={()=>{}} onDelete={()=>{}}/>
      </Link>
      <div>{selectedStarCount}</div>
      <HeaderList toPaths={toPaths}>{listItems}</HeaderList>
      <Inst onClick={() => console.log("click")} />
      <Threads onClick={() => console.log("click")} />
    </Head>
  );
}

export default Header;
