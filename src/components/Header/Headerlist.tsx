import React from 'react'
import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';

export const ListHeader = styled.ul`
display: flex;
justify-content: flex-end;;
    width: 85vw;`

export const Link = styled(RouterLink)`
color: #16342C;   
  text-align: center;
  font-family: 'Playfair Display', serif;
  font-size: 20px;
  font-style: normal;
  font-weight: 400;
  text-decoration: none;
  margin-right: 3vw;` 

interface Props{
    toPaths: string[];
    children: string[];
}
function HeaderList({toPaths, children}:Props) {
  return (
    <ListHeader>
        {children.map((item, index)=>(
        <Link key={index} to={toPaths[index]}>{item}</Link>
        ))}
    </ListHeader>
  )
}

export default HeaderList