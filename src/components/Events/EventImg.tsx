import React from 'react'
import styled from "styled-components";

export const Img = styled.img`
width: 310px;
height: 200px;
box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
`

interface Props{
    src: string;
    alt: string;
}
function EventImg({src, alt}: Props) {
  return (
    <Img src={src} alt={alt}></Img>
  )
}

export default EventImg