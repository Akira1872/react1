import React, { useState, useEffect } from 'react';
import Cart from './Cart';
import DeleteSvg from './DeleteSvg';
import Star from './Star';
import Img from './Img';
import Price from "./Price";
import Name from './Name';
import Color from "./Color";
import { CardS, CardPrice, BoxPrice} from "./CardStyles";


interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

interface CardProps {
  data: DataItem;
  onClick: () => void;
  cartClick: ()=>void;
  onDelete: () => void;
  hideCart?: boolean;
  hideDelete?: boolean;
  deleteClick: ()=>void;
}

function Card({ data, onClick, cartClick, onDelete, hideCart = false, hideDelete = false, deleteClick}: CardProps) {

  return (
    <CardS className={`card${data.artikel}`}>
      <Img src={data.src} alt={data.name} />
      <CardPrice>
      {!hideCart && <Cart cartClick={cartClick} colored={true}/>}
      {!hideDelete && <DeleteSvg deleteClick={deleteClick}/>}
        <Star onClick={onClick} id={data.artikel} colored={true} onDelete={onDelete}/>
        <BoxPrice>
          <Price>{data.price}</Price>
          <Color>{data.color}</Color>
          <Name>{data.name}</Name>
        </BoxPrice>
      </CardPrice>
    </CardS>
  );
}

export default Card;