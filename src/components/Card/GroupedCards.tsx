import React from "react";
import { CardContainer, GroupBox, CardListBox, GroupTitle } from "./CardStyles";
import Card from "./Card";

interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

interface Props {
  groupedData: { [key: string]: DataItem[] };
  onClick: (artikel: string) => void;
  cartClick: (artikel: string)=>void;
}

function GroupedCards({ groupedData, onClick, cartClick}: Props) {
  return (
    <CardContainer>
      {Object.keys(groupedData).map((color) => (
        <GroupBox key={color}>
          <GroupTitle>{color} Flower</GroupTitle>
          <CardListBox>
            {groupedData[color].map((data) => (
              <Card
                key={data.artikel}
                data={data}
                onClick={() => onClick(data.artikel)}
                cartClick={()=> cartClick(data.artikel)}
                hideDelete={true}
                onDelete={()=>{}}
                deleteClick={()=>{}}
              />
            ))}
          </CardListBox>
        </GroupBox>
      ))}
    </CardContainer>
  );
}

export default GroupedCards;
