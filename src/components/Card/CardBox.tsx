import React, { useState, useEffect } from "react";
import { fetchData } from "../Api";
import * as styles from "./CardStyles";
import FirstModal from "../Modal/FirstModal";
import SecondModal from "../Modal/SecondModal";
import Filtr from "./Filtr";
import GroupedCards from "./GroupedCards";

interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

interface Props {
  onClick: (artikel: string) => void;
  firstClick: (artikel: string) => void;
}

function Cards({onClick, firstClick}: Props) {

  const [data, setData] = useState<DataItem[]>([]);

  const [modals, setModals] = useState<{ [key: string]: boolean }>({
    first: false,
    second: false,
  });

  const [selectedArtikel, setSelectedArtikel] = useState<string>("");
  const [selectedCartItem, setSelectedCartItem] = useState<string>("");

  useEffect(() => {
    fetchData()
      .then((data) => {
        setData(data);
      })
      .catch((err) => {
        console.error("Error receiving data", err);
      });
  }, []);

   const toggleModal = (modalName: string) => {
    setModals({ ...modals, [modalName]: !modals[modalName] });
  };

  const closeModal = () => {
    setModals({ ...modals, first: false, second: false }); 
  };

  const handleSvgClick = (artikel: string) => {
    const selectedItems = JSON.parse(localStorage.getItem("selectedModalItems") || "[]");
    const isSelected = selectedItems.includes(artikel);

    if (!isSelected) {
      setSelectedArtikel(artikel);
      toggleModal("second"); 
    } else {
      setSelectedCartItem(artikel);
      toggleModal("first"); 
    }
  };

  const groupByColor = (data: DataItem[]) => {
    const groupedData: { [key: string]: DataItem[] } = {};
    data.forEach((item) => {
      const color = item.color;
      if (!groupedData[color]) {
        groupedData[color] = [];
      }
      groupedData[color].push(item);
    });
    return groupedData;
  };

  const groupedData = groupByColor(data);

  const listItems = [
    "Bouquets",
    "Compositions ",
    "Wedding bouquets ",
    "Indoor flowers ",
  ];



  return (
    <styles.CardContainer>
      <Filtr onClick={() => console.log("click")}>{listItems}</Filtr>
      <GroupedCards groupedData={groupedData} onClick={onClick}  cartClick={(artikel)=>{handleSvgClick(artikel)}}/>
      {modals.first && <FirstModal closeModal={closeModal} firstClick={()=>firstClick(selectedArtikel)} selectedCartItem={selectedCartItem} data={data.find(item => item.artikel === selectedArtikel)} />}
      {modals.second && <SecondModal closeModal={closeModal} firstClick={() => firstClick(selectedArtikel)} data={data.find(item => item.artikel === selectedArtikel)}/>}
    </styles.CardContainer>
  );
}

export default Cards;