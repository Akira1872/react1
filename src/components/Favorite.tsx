import React, { useState, useEffect } from "react";
import { fetchData } from "./Api";
import Card from "./Card/Card";
import FooterContainer from "./Footer/FooterContainer";

interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

interface Props{
  onClick: (artikel:string)=>void;
}

type FindProduct = (artikel: string) => DataItem | undefined;

function Favorite({onClick}:Props) {

  const [data, setData] = useState<DataItem[]>([]);

  const [selectedItems, setSelectedItems] = useState([]);

  useEffect(() => {
    fetchData()
      .then((data) => {
        setData(data);
      })
      .catch((err) => {
        console.error("Error receiving data", err);
      });

    const storedItems = JSON.parse(localStorage.getItem(`selectedStarItems`) || '[]');

    setSelectedItems(storedItems);

  }, []);

  const findProduct: FindProduct = (artikel) => {
    return data.find((item) => item.artikel === artikel);
  };

  const handleCardDelete = (artikelToDelete:string) => {
    setSelectedItems((prevItems) =>
      prevItems.filter((item) => item !== artikelToDelete)
    );
  };

  return (
    <div className="container">
      <div className="con">
      {selectedItems.map((artikel, index) => {
        const selectedItemData = findProduct(artikel); 
        if (selectedItemData) {
          return (
            <Card
              key={index}
              data={selectedItemData}
              onClick={()=>onClick(artikel)} 
              cartClick={()=>console.log('k')} 
              onDelete={() => handleCardDelete(artikel)}
              hideCart={true}
              hideDelete={true}
              deleteClick={()=>{}}
            />
          );
        }
        return null;
      })}
      </div>
      <FooterContainer/>
    </div>
  );
}

export default Favorite;