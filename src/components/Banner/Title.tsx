import React from 'react'
import styled from 'styled-components';

export const TitleHeader = styled.h1`
color: #2E1D08;   
  text-align: center;
  font-family: Montserrat;
  font-size: 40px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  margin-bottom: 2vh;
`
interface Props{
    children?: string;
}
function Title({children}:Props) {
  return (
    <TitleHeader>{children}</TitleHeader>
  )
}

export default Title