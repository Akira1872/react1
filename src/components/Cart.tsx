import React, { useState, useEffect } from "react";
import { fetchData } from "./Api";
import Card from "./Card/Card";
import FooterContainer from "./Footer/FooterContainer";
import FirstModal from "./Modal/FirstModal";

interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

interface Props{
  onClick: (artikel:string)=>void;
  firstClick: (artikel:string)=>void;
}
type FindProduct = (artikel: string) => DataItem | undefined;

function Cart({onClick, firstClick}:Props) {

  const [modals, setModals] = useState<{ [key: string]: boolean }>({
    first: false,
  });
  
  const [data, setData] = useState<DataItem[]>([]);

  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedCartItem, setSelectedCartItem] = useState<string>("");

  useEffect(() => {
    fetchData()
      .then((data) => {
        setData(data);
      })
      .catch((err) => {
        console.error("Error receiving data", err);
      });

    const storedItems = JSON.parse(localStorage.getItem(`selectedModalItems`) || '[]');
    setSelectedItems(storedItems);

  }, []);


  const findProduct: FindProduct = (artikel) => {
    return data.find((item) => item.artikel === artikel);
  };

  const toggleModal = (modalName: string, artikel?: string) => {
    if (modalName === "first" && artikel) {
      setSelectedCartItem(artikel);
    }
    setModals({ ...modals, [modalName]: !modals[modalName] });
  };

  const closeModal = () => {
    setModals({ first: false, second: false });
  };

  const handleCardDelete = (artikelToDelete:string) => {
    setSelectedItems((prevItems) =>
      prevItems.filter((item) => item !== artikelToDelete)
    );
    firstClick(artikelToDelete);
  };

 

  return (
    <div className="container">
     <div className="con">
     {selectedItems.map((artikel, index) => {
        const selectedItemData = findProduct(artikel); 
        if (selectedItemData) {
          return (
            <Card
              key={index}
              data={selectedItemData}
              onClick={()=>onClick(artikel)} 
              cartClick={()=>{}} 
              onDelete={()=>{}}
              deleteClick={()=>toggleModal("first", artikel)}
              hideCart={true}

            />
          );
        }
        return null;
      })}
     </div>
       {modals.first && <FirstModal closeModal={closeModal} firstClick={handleCardDelete} selectedCartItem={selectedCartItem} data={findProduct(selectedCartItem)} />}
      <FooterContainer/>
    </div>
  );
}

export default Cart
