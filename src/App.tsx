import React, { useState, useEffect } from "react";
import Home from "./components/Home";
import { Route, Routes } from "react-router-dom";
import NotFound from "./components/NotFound";
import Header from "./components/Header/Header";
import Cart from "./components/Cart";
import Favorite from "./components/Favorite";



function App() {
  const [selectedStarCount, setSelectedStarCount] = useState(0);
  const [selectedModalCount, setSelectedModalCount] = useState(0);

  const updateSelectedCount = (type: string) => {
    const selectedItems = JSON.parse(localStorage.getItem(`selected${type}Items`) || '[]');
    switch (type) {
      case 'Star':
        setSelectedStarCount(selectedItems.length);
        break;
      case 'Modal':
        setSelectedModalCount(selectedItems.length);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    updateSelectedCount('Star');
    updateSelectedCount('Modal');
  }, []);

  const handleSvg = (artikel: string, type: string) => {
    const selectedItems = JSON.parse(localStorage.getItem(`selected${type}Items`) || '[]');
    
    const isSelected = selectedItems.includes(artikel);
    const updatedItems = isSelected 
      ? selectedItems.filter((i:string) => i !== artikel)
      : [...selectedItems, artikel];

    localStorage.setItem(`selected${type}Items`, JSON.stringify(updatedItems));
    updateSelectedCount(type);
  };
  

  const handleStarClick = (artikel: string) => {
    handleSvg(artikel, 'Star');
  };

  const handleModalButtonClick = (artikel: string) => {
    handleSvg(artikel, 'Modal');
  };

  // localStorage.clear()

  return (
    <div className="container">
    <Header selectedStarCount={selectedStarCount} selectedModalCount={selectedModalCount}/>
    <Routes>
      <Route path="/" element={<Home onClick={handleStarClick} firstClick={handleModalButtonClick}/>}/>
      <Route path="/cart" element={<Cart onClick={handleStarClick} firstClick={handleModalButtonClick}/>}/>
      <Route path="/star" element={<Favorite onClick={handleStarClick}/>}/>
      <Route path="*" element={<NotFound/>}/>
    </Routes>
    </div>
  );
}

export default App;
