import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    selectedStarCount: 0,
    selectedModalCount: 0,
  };

const counterSlice = createSlice({
    name: "counter", 
    initialState,
    reducers: {
        updateSelectedCount: (state, action) => {
            const { selectedStarCount, selectedModalCount } = action.payload;
            state.selectedStarCount = selectedStarCount;
            state.selectedModalCount = selectedModalCount;
        },
    }
})


export const { updateSelectedCount } = counterSlice.actions;
export default counterSlice.reducer;