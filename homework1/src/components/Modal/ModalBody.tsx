import React from 'react';
import styled from "styled-components";

export const ModalB = styled.div`
  color: #7F7F7F;   
  text-align: center;
  font-family: 'Playfair Display',serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;`;

interface Props{
    children: string;
}

function ModalBody({children}:Props) {
  return (
    <ModalB>{children}</ModalB>
  )
}

export default ModalBody