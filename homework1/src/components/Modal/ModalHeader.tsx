import React, { Children } from 'react';
import styled from "styled-components";

export const ModalH = styled.div` text-align: center;
color: #EEAE5E;
font-family: 'Playfair Display',serif;
font-size: 32px;
font-style: normal;
font-weight: 500;
line-height: normal;`;

interface Props{
    children: string;
}

function ModalHeader({children}:Props) {
  return (
    <ModalH>{children}</ModalH>
  )
}

export default ModalHeader