import React, { useState } from "react";
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import Modal from "./Modal";
import "./Style.scss";

interface DataItem {
  name: string;
  price: string;
  src: string;
  artikel: string;
  color: string;
}

interface Props {
  data?: DataItem;
  isOpen?: boolean;
  closeModal: () => void;
  firstClick: () => void;
}

function SecondModal({ isOpen, closeModal, firstClick}: Props) {
  const [clicked, setClicked] = useState(false);
  
  const closeModalHandler = () => {
    closeModal();
  };

  const btnClick = () =>{
    firstClick();
    closeModal();
    setClicked(true);
  }
  

  return (
    <Modal onClick={closeModal}>
      <ModalWrapper
        type="second"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <ModalClose onClick={closeModalHandler} />
        <ModalHeader>Add Product</ModalHeader>
        <ModalBody>Description for you product</ModalBody>
        <ModalFooter
          firstText="ADD TO FAVORITE"
          firstClick={btnClick}
        />
      </ModalWrapper>
    </Modal>
  );
}

export default SecondModal;
