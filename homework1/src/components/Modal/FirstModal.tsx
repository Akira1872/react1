import React, { useState } from "react";
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import Modal from "./Modal";
import Img from "./Img";
import "./Style.scss";

interface Props {
  isOpen?: boolean;
  closeModal: () => void;
  firstClick: (artikel:string)=>void;
  selectedCartItem: string;
}

function FirstModal({ isOpen, closeModal, firstClick, selectedCartItem}: Props) {
  const closeModalHandler = () => {
    closeModal();
  };

  const btnFirstClick = ()=>{
    closeModal();
  }

  const btnSecondClick = ()=>{
    closeModal();
    firstClick(selectedCartItem);
  }

  return (
    <Modal onClick={closeModal}>
      <ModalWrapper
        type="first"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <ModalClose onClick={closeModalHandler} />
        <Img />
        <ModalHeader>Remove from the shopping cart ?</ModalHeader>
        <ModalBody>
          By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.
        </ModalBody>
        <ModalFooter
          firstText="NO, CANCEL"
          firstClick={btnFirstClick}
          secondText="YES, DELETE"
          secondaryClick={btnSecondClick}
        />
      </ModalWrapper>
    </Modal>
  );
}

export default FirstModal;
