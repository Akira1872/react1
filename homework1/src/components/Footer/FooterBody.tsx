import React from 'react'
import styled from 'styled-components'

export const FooterB = styled.div`
  color: #EEAE5E;   
  text-align: center;
  font-family: 'Playfair Display',serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;`;

  interface Props{
    children: string;
}

function FooterBody({children}:Props) {
  return (
    <FooterB>{children}</FooterB>
  )
}

export default FooterBody