import React from 'react'
import styled from 'styled-components';

export const ListF = styled.ul`
width: 80%;
height: 3vh;
display: flex;
justify-content: space-evenly;
margin: 8vh;
`

export const LinkF = styled.a`
color: #16342C;   
  text-align: center;
  font-family: 'Playfair Display', serif;
  font-size: 17px;
  font-style: normal;
  font-weight: 400;
  text-decoration: none;
  margin-right: 3vw;
  width: 23vw;
  height: 3vh;
  border-bottom: solid 1px;
 ` 

interface Props{
    onClick: ()=> void;
    children: string[];
}
function Filtr({onClick, children}:Props) {
  return (
    <ListF>
        {children.map((item, index)=>(
        <LinkF key={index} href="#" onClick={onClick}>{item}</LinkF>
        ))}
    </ListF>
  )
}

export default Filtr