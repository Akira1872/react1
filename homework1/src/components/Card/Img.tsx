import React from 'react'
import styled from "styled-components";

export const CardImg = styled.img`
width: 200px;
height: 200px;
border-top-left-radius: 16px;
border-top-right-radius: 16px;
`

interface Props{
  src: string;
  alt: string;
}

function Img({src, alt}:Props) {
  return (
    <CardImg src={src} alt={alt} />
  )
}

export default Img