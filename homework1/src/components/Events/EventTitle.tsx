import React from 'react'
import styled from 'styled-components';

export const TitleE = styled.a`
color: #16342C;   
  text-align: center;
  font-family: 'Playfair Display', serif;
  font-size: 35px;
  font-style: normal;
  font-weight: 400;
  text-decoration: none;
  margin: 1vw;
  `;

interface Props{
    children: string;
    onClick: ()=> void;

}
function EventTitle({children, onClick}:Props) {
  return (
   <TitleE href="#" onClick={onClick}>{children}</TitleE>
  )
}

export default EventTitle